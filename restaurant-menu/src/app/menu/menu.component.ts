import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  constructor(private router: Router) {}
  menuItems = [
    { id: 1, name: 'Burger', description: 'A classic hamburger',
    price: 5.99 },
    { id: 2, name: 'Fries', description: 'Crispy french fries',
    price: 2.99 },
    { id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
    }
    ];

    showItem(idInput:string):void{
      this.router.navigate(['/menuitem'],
      {
      queryParams: {
        id: idInput
      }
      }
      );
    }
}
