import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menuitem',
  templateUrl: './menuitem.component.html',
  styleUrls: ['./menuitem.component.css']
})
export class MenuitemComponent {
  menuItem!: any; 
  menuItems = [
    { id: 1, name: 'Burger', description: 'A classic hamburger',
    price: 5.99 },
    { id: 2, name: 'Fries', description: 'Crispy french fries',
    price: 2.99 },
    { id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
    }
    ];

    constructor(
      private activatedRoute: ActivatedRoute,
      
    ) { }
    
    ngOnInit() {
      this.activatedRoute.queryParams.subscribe((params) => {
        let id = params['id'];
        this.menuItem = this.getItem(id);
        
      });
    }
    getItem(id: number) {
      return this.menuItems.find((menuItems) => menuItems.id == id);
    }
}
